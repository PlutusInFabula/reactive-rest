# README #

This is an immature study on how to manage Asyncron REST calls with Spring and Reactor

### What is this repository for? ###

* Quick summary
* 0,1

### How do I get set up? ###

* Import in Intellij Idea
* run
* Access : http://localhost:8080/getGreeting/someName for a basic example with parallel consumers
* Access : http://localhost:8080/threeSteps/someName for a chain of events example

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact