package hu.idomsoft.kf.reactor.REST.BasicEsample;

import hu.idomsoft.kf.reactor.REST.EventWrapper;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.bus.EventBus;
import reactor.spring.context.annotation.Consumer;
import reactor.spring.context.annotation.Selector;
import reactor.spring.context.annotation.SelectorType;

/**
 * Created by Pluto on 2015. 11. 22..
 */
@Consumer
public class EventConsumer {

    @Autowired
    public EventBus eventBus;

    @Selector(value = ".*.topic", type = SelectorType.REGEX)
    public void topicLogger(EventWrapper ew) {
        System.out.println("Event receved for Logging " + ew.toString());
        System.out.println("Payload: " + ReflectionToStringBuilder.toString(ew.getPayload(), ToStringStyle.JSON_STYLE));
    }

    @Selector("greeting.topic")
    public void wrappedGreeter(EventWrapper<String, String> ew) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ew.getDeferredResult().setResult("Ciao " + ew.getPayload().toUpperCase());
    }
}
