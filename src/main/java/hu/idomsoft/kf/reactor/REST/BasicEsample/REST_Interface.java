package hu.idomsoft.kf.reactor.REST.BasicEsample;

import hu.idomsoft.kf.reactor.REST.EventWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;
import reactor.bus.Event;
import reactor.bus.EventBus;

/**
 * Created by Pluto on 2015. 11. 22..
 */
@RestController
public class REST_Interface {

    @Autowired
    public EventBus eventBus;

    @RequestMapping("/getGreeting/{name}")
    public DeferredResult<String> getGreeting(@PathVariable String name) {

        DeferredResult<String> result = new DeferredResult<>();

        EventWrapper<String, String> wrapper = new EventWrapper<>(result, name);
        eventBus.notify("greeting.topic", Event.wrap(wrapper));
        //In one line...
        // eventBus.notify("greeting.topic", Event.wrap(new EventWrapper<String>(result, name)));
        return result;
    }

}
