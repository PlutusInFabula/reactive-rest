package hu.idomsoft.kf.reactor.REST.ComplexObjectEvent;

/**
 * Created by Pluto on 2015. 11. 22..
 */
public class Car {
    private int seat;
    private String colour;
    private Integer speeds;

    public Car(int seat, String colour, Integer speeds) {
        this.seat = seat;
        this.colour = colour;
        this.speeds = speeds;
    }

    public int getSeat() {
        return seat;
    }

    public void setSeat(int seat) {
        this.seat = seat;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public Integer getSpeeds() {
        return speeds;
    }

    public void setSpeeds(Integer speeds) {
        this.speeds = speeds;
    }
}
