package hu.idomsoft.kf.reactor.REST.ComplexObjectEvent;

import hu.idomsoft.kf.reactor.REST.EventWrapper;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.bus.EventBus;
import reactor.spring.context.annotation.Consumer;
import reactor.spring.context.annotation.Selector;

/**
 * Created by Pluto on 2015. 11. 22..
 */
@Consumer
public class ComplexObject_EventConsumer {

    @Autowired
    public EventBus eventBus;

    @Selector(value = "ComplexObject")
    public void topicLogger(EventWrapper ew) {
        System.out.println("Event receved for Logging " + ew.toString());
        System.out.println("Payload: " + ReflectionToStringBuilder.toString(ew.getPayload(), ToStringStyle.JSON_STYLE));
    }

    @Selector("ComplexObject")
    public void wrappedGreeter(EventWrapper<String, Car> ew) {
        ew.getDeferredResult().setResult("Max Speed " + ew.getPayload().getSpeeds().toString());
    }
}
