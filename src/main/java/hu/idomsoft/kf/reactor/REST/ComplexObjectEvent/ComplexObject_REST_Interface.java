package hu.idomsoft.kf.reactor.REST.ComplexObjectEvent;

import hu.idomsoft.kf.reactor.REST.EventWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;
import reactor.bus.Event;
import reactor.bus.EventBus;

/**
 * Created by Pluto on 2015. 11. 22..
 */
@RestController
public class ComplexObject_REST_Interface {

    @Autowired
    public EventBus eventBus;

    @RequestMapping("/drive/")
    public DeferredResult<String> drive() {

        Car car = new Car(2, "black", 240);

        DeferredResult<String> result = new DeferredResult<>();
        EventWrapper<String, Car> wrapper = new EventWrapper<>(result, car);
        eventBus.notify("ComplexObject", Event.wrap(wrapper));
        return result;
    }

}
