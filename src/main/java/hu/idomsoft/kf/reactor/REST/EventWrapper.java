package hu.idomsoft.kf.reactor.REST;

import org.springframework.web.context.request.async.DeferredResult;

/**
 * Created by Pluto on 2015. 11. 22..
 */
public class EventWrapper<V, T> {
    private DeferredResult<V> deferredResult;
    private T payload;

    public EventWrapper(DeferredResult<V> deferredResult, T payload) {
        this.deferredResult = deferredResult;
        this.payload = payload;
    }

    public DeferredResult<V> getDeferredResult() {
        return deferredResult;
    }

    public void setDeferredResult(DeferredResult<V> deferredResult) {
        this.deferredResult = deferredResult;
    }

    public T getPayload() {
        return payload;
    }

    public void setPayload(T payload) {
        this.payload = payload;
    }
}
