package hu.idomsoft.kf.reactor.REST.threeStepsExample;

import hu.idomsoft.kf.reactor.REST.EventWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.bus.Event;
import reactor.bus.EventBus;
import reactor.spring.context.annotation.Consumer;
import reactor.spring.context.annotation.Selector;

/**
 * Created by Pluto on 2015. 11. 22..
 */
@Consumer
public class Steps_EventConsumer {

    @Autowired
    public EventBus eventBus;

    @Selector("3step.first")
    public void firstStep(EventWrapper<String, String> ew) {
        String modifiedPayload = ew.getPayload().toUpperCase();
        eventBus.notify("3step.second", Event.wrap(new EventWrapper<String, String>(ew.getDeferredResult(), modifiedPayload)));
    }

    @Selector("3step.second")
    public void secondStep(EventWrapper<String, String> ew) {
        String modifiedPayload = "Buongiorno " + ew.getPayload();
        eventBus.notify("3step.third", Event.wrap(new EventWrapper<String, String>(ew.getDeferredResult(), modifiedPayload)));
    }
}
