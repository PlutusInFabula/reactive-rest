package hu.idomsoft.kf.reactor.REST.threeStepsExample;

import hu.idomsoft.kf.reactor.REST.EventWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.bus.EventBus;
import reactor.spring.context.annotation.Consumer;
import reactor.spring.context.annotation.Selector;

/**
 * Created by Pluto on 2015. 11. 22..
 */
@Consumer
public class Steps_OtherEventConsumer {

    @Autowired
    public EventBus eventBus;

    @Selector("3step.third")
    public void secondStep(EventWrapper<String, String> ew) {
        String modifiedPayload = ew.getPayload() + "! This is a good day.";
        ew.getDeferredResult().setResult(modifiedPayload);
    }
}
