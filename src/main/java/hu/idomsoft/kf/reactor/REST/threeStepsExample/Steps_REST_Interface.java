package hu.idomsoft.kf.reactor.REST.threeStepsExample;

import hu.idomsoft.kf.reactor.REST.EventWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;
import reactor.bus.Event;
import reactor.bus.EventBus;

/**
 * Created by Pluto on 2015. 11. 22..
 */
@RestController
public class Steps_REST_Interface {

    @Autowired
    public EventBus eventBus;

    @RequestMapping("/threeSteps/{name}")
    public DeferredResult<String> getGreeting(@PathVariable String name) {

        DeferredResult<String> result = new DeferredResult<>();

        eventBus.notify("3step.first", Event.wrap(new EventWrapper<String, String>(result, name)));
        return result;
    }

}
