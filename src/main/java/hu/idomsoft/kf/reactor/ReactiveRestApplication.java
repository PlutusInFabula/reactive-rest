package hu.idomsoft.kf.reactor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import reactor.Processors;
import reactor.bus.EventBus;
import reactor.spring.context.config.EnableReactor;

@SpringBootApplication
@EnableAutoConfiguration
@EnableReactor
public class ReactiveRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReactiveRestApplication.class, args);
    }

    @Bean
    public EventBus eventBus() {
        return EventBus.config()
                .processor(Processors.topic())
                .get();
    }
}
